/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-07 14:32:48
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-13 17:26:22
 * @FilePath: /vue-create/rollup.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import babel from "rollup-plugin-babel";
import serve from "rollup-plugin-serve";
export default {
  input: "./src/index.js",
  output: {
    file: "dist/vue.js",
    format: "umd", // 在window上挂载一个全局vue对象
    name: "Vue",
    sourcemap: true,
  },
  plugins: [
    babel({ exclude: "node_modules/**" }),
    serve({
      port: 3000,
      contentBase: "", // 当前目录
      openPage: "/index.html",
    }),
  ],
};

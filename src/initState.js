/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-08 14:39:16
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-23 17:34:05
 * @FilePath: /vue-create/src/initState.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { observer } from "./observe/index.js";
import { Watcher } from "./observe/watcher.js";
import { nextTick } from "./utils/nextTick";
export function initState(vm) {
  let opts = vm.$options;
  // 判断
  if (opts.props) {
    initProps();
  }
  if (opts.data) {
    initData(vm);
  }
  if (opts.watch) {
    initWatch(vm);
  }
  if (opts.computed) {
    initComputed(vm);
  }
  if (opts.methods) {
    initMethods();
  }
}
function initProps() { }

// vue2 对data进行初始化
function initData(vm) {
  let data = vm.$options.data;
  data = vm._data =
    typeof data === "function"
      ? data.apply(
        vm
      ) /**必须加apply/call 改变函数内部this指向，否则vue的data函数this指向window对象 */
      : data;
  // 把data对象所有属性代理到vm上，方便在vue实例内通过this.xxx来访问和修改属性
  for (let key in data) {
    proxy(vm, "_data", key);
  }
  // 数据劫持
  /**
   * 分两种情况:
   * 1:对象
   * 2:数组
   */
  observer(data);
}

// 自定义的代理，不是 es6提供的Proxy
function proxy(vm, source, key) {
  Object.defineProperty(vm, key, {
    get() {
      return vm[source][key];
    },
    set(newValue) {
      vm[source][key] = newValue;
    },
  });
}
function initWatch(vm) {
  let watch = vm.$options.watch
  for (let key in watch) {
    let handler = watch[key]
    if (Array.isArray(handler)) {
      handler.forEach(item => createWatcher(vm, key, item))
    } else {
      /**创建一个方法来处理 */
      createWatcher(vm, key, handler)
    }
  }
}
function initComputed() { }
function initMethods() { }
/**
 * 
 * @param {*} vm 
 * @param {*} expOrfn : 对应 vm.$watch(()=>{return 'a'})  返回值就是watcher上的属性
 * @param {*} handler 
 * @param {*} options 
 */
function createWatcher(vm, expOrfn, handler, options) {
  if (typeof handler === 'object') {
    options = handler // 用户的配置项
    handler = handler.handler
  }
  if (typeof handler === 'string') {
    // `type:'changeType'`. changeType in methods
    handler = vm[handler] // 将实例上的方法

  }
  // typeof handler == function  最终处理$watch 这个方法
  return vm.$watch(vm, expOrfn, handler, options)
}
export function stateMixin(vm) {
  vm.prototype.$nextTick = function (cb) {
    nextTick(cb)
  }
  vm.prototype.$watch = function (Vue, expOrfn, handler, options) {
    // watch 实现的地方
    console.log({ expOrfn, handler, options })
    let watcher = new Watcher(Vue, expOrfn, handler, { ...options, user: true /**表示用户的属性 */ })
    if (options && options.immediate) {
      handler.call(Vue) // 如果有这个immediate属性，立即执行handler
    }
  }
}

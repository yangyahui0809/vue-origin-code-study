/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-04-14 15:13:39
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-25 18:11:15
 * @FilePath: /vue-create/src/vnode/patch.js
 * @Description: 
 * 
 * Copyright (c) 2023 by yangyahui, All Rights Reserved. 
 */

/**
 * 更新数据： vue diff 算法 最小量更新（比对） old new vnode
 * 1 创建两个vnode
 * 2 比对：在数据更新的时候，拿到老的节点和新的节点做对比，将不同的地方进行更新
 */
export function patch(oldVnode, vnode) {
    if (oldVnode.nodeType === 1) {
        // console.log(oldVnode, vnode)
        let el = createEl(vnode)
        // console.log(el, '--------')
        // 替换 ：1，获取父节点 2，把新的节点插入 3，把老元素删除
        let parentEl = oldVnode.parentNode
        parentEl.insertBefore(el, oldVnode.nextsibling)
        parentEl.removeChild(oldVnode)
        return el
    } else {
        // 1.元素不一样
        // node.tag 不同直接替换子树
        if (oldVnode.tag !== vnode.tag) {
            oldVnode.el.parentNode.replaceChild(createEl(vnode), oldVnode.el)
        }
        // 2.标签一样，text和属性不一样
        if (!oldVnode.tag) {
            if (oldVnode.text !== vnode.text) {
                return oldVnode.el.textContent = vnode.text
            }
        }
        // 2.1属性 不同
        // 直接复制
        let el = vnode.el = oldVnode.el
        updateProps(vnode, oldVnode.data)
        // diff 子元素 ： 
        let oldChildren = oldVnode.children || []
        let newChildren = vnode.children || []

        if (oldChildren.length > 0 && newChildren.length > 0) {
            // 新老都有儿子 创建一个方法
            updateChild(oldChildren, newChildren, el)
        } else if (oldChildren.length > 0) {
            // 老元素有儿子，新的没有儿子
            el.innerHTML = ''
        } else if (newChildren.length > 0) {
            // 老的没有儿子，新的有
            for (let i = 0; i < newChildren.length; i++) {
                let child = newChildren[i]
                el.appendChild(createEl(child))
            }
        }


    }

}
// 双指针
function updateChild(oldChildren, newChildren, parent) {
    /**
     * diff 策略：
     * 尾部添加 头部添加
     * 倒叙和正序遍历
     * 原则的就地复用，最小量更新
     */
    /** 创建双指针 */
    let oldStartIndex = 0 //旧元素的开头索引和开始元素
    let oldStartVnode = oldChildren[oldStartIndex]
    let oldEndIndex = oldChildren.length - 1
    let oldEndvnode = oldChildren[oldEndIndex]

    let newStartIndex = 0 //新元素的开头索引和开始元素
    let newStartVnode = newChildren[newStartIndex]
    let newEndIndex = newChildren.length - 1
    let newEndvnode = newChildren[newEndIndex]
    function isSameVnode(oldContent, newContent) {
        return (oldContent.tag === newContent.tag) && (oldContent.key === newContent.key)
        /**
         * 面试题：为什么要添加key  && 这个key为什么不能是数组索引index？
         * 结论:diff通过key进行比对，可以减少同tag的dom比对消耗。
         * 至于不能key不能是数组index,因为如果是初次渲染没问题，但是如果动态变更，比如排序操作就会影响性能
         * <li key="0">children_a</li>
         * <li key="1">children_b</li>
         * <li key="2">children_c</li>
         * --------diff--------
         * <li key="0">children_c</li>
         * <li key="1">children_b</li>
         * <li key="2">children_a</li>
         * --------reason------
         * 此时 isSameVnode() === true，因为tag和key均一致，会走正序diff递归,发现内部children不同时会走patch内部的dom创建,虽然能正确渲染，但是会进行开销高的dom销毁创过程。
         * 如果用唯一随机id表示，则会走暴力比对，元素排序操作只需要上下移动元素即可
         */
    }
    /**正序比对 */
    while (oldStartIndex <= oldEndIndex && newStartIndex <= newEndIndex) {
        // 对比子元素 头部元素是不是同一个元素
        const kk = isSameVnode(oldStartVnode, newStartVnode)
        console.log(kk, 'kkkkk')
        if (isSameVnode(oldStartVnode, newStartVnode)) {
            // 正序递归对比
            patch(oldStartVnode, newStartVnode)
            // 移动指针
            oldStartVnode = oldChildren[++oldStartIndex]
            newStartVnode = newChildren[++newStartIndex]
        } else if (isSameVnode(oldEndvnode, newEndvnode)) {
            /**逆序对比 */
            patch(oldEndvnode, newEndvnode)
            oldEndvnode = oldChildren[--oldEndIndex]
            newEndvnode = newChildren[--newEndIndex]
        } else if (isSameVnode(oldStartVnode, newEndvnode)) {
            /**正交叉对比 */
            patch(oldStartVnode, newEndvnode)
            oldStartVnode = oldChildren[++oldStartIndex]
            newEndvnode = newChildren[--newEndIndex]
        } else if (isSameVnode(oldEndvnode, newStartVnode)) {
            /**逆交叉对比 */
            patch(oldEndvnode, newStartVnode)
            oldEndvnode = oldChildren[--oldEndIndex]
            newStartIndex = newChildren[++newStartIndex]
        } else {
            /**开头比对均失败，则进行暴力对比 */
        }
    }
    // 添加多余的儿子
    if (newStartIndex <= newEndIndex) {
        for (let i = oldStartIndex; i <= newEndIndex; i++) {
            parent.appendChild(createEl(newChildren[i]))
        }
    }
}
// 添加属性
function updateProps(vnode, oldProps = {}) { // 第一次的属性
    let newProps = vnode.data || {} // 获取当前属性的节点
    let el = vnode.el // 获取当前真实节点

    // oldvnode 有属性，新的没有
    for (let key in oldProps) {
        if (!newProps[key]) {
            el.removeAttribute(key)
        }
    }
    // 样式对比
    let newStyle = newProps.style || {}
    let oldStyle = oldProps.style || {}
    for (let key in oldStyle) {
        if (!newStyle[key]) {
            el.style = ''
        }
    }
    // newvnode 有属性，老的没有    操作真实dom
    for (let key in newProps) {
        if (key === "style") {
            for (let styleName in newProps.style) {
                el.style[styleName] = newProps.style[styleName]
            }
        } else if (key === 'class') {
            el.className = newProps.class
        } else {
            el.setAttribute(key, newProps[key])
        }
    }
}
// 创建真实dom
export function createEl(vnode) {
    let { tag, data, key, children, text } = vnode
    if (typeof tag === 'string') {
        vnode.el = document.createElement(tag)
        updateProps(vnode)
        if (children && children.length > 0) {
            children.forEach(child => {
                // 递归
                vnode.el.appendChild(createEl(child))
            })
        }
    } else {
        vnode.el = document.createTextNode(text)
    }
    return vnode.el
}

/**
 * 面试题：
 * vue渲染流程：
 * 数据初始化
 * 对模版通过正则匹配和FILO入栈出栈操作替代递归进行编译得到ast语法树
 * 把ast语法树递归拼接成函数字符串（_c,_v,_s）
 * 听过new Function(`with(this){return ${code}}`)变成render函数
 * 实现 render函数内部的_c,_v,_s方法并挂载到原型链上，通过with实现无namespace的方法调用
 * 通过render函数（内部递归调用已经实现的_c,_v,_s方法）生成vnode
 * vnode变成真实dom（递归调用原生创建元素方法）
 * 放到页面上去
 */
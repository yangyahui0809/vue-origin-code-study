/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-04-11 13:38:05
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-19 12:24:07
 * @FilePath: /vue-create/src/vnode/index.js
 * @Description: 
 * 
 * Copyright (c) 2023 by yangyahui, All Rights Reserved. 
 */
export function renderMixin(Vue) {
    Vue.prototype._c = function () {
        return createElement(...arguments)
    }
    Vue.prototype._v = function (text) {
        return createText(text)
    }
    Vue.prototype._s = function (val) {
        return val === null ? "" : (typeof val === 'object') ? JSON.stringify(val) : val
    }
    Vue.prototype._render = function () {
        // debugger
        let vm = this
        let render = vm.$options.render
        let vnode = render.call(this)
        return vnode
    }
}

// 创建元素
function createElement(tag, data = {}, ...children) {
    return vnode(tag, data, data.key, children)
}
// 创建文本
function createText(text) {
    return vnode(undefined, undefined, undefined, undefined, text)
}
// 创建vnode
function vnode(tag, data, key, children, text) {
    return {
        tag, data, key, children, text
    }
}

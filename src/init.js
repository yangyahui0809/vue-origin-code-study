/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-08 14:34:50
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-18 19:34:38
 * @FilePath: /vue-create/src/init.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

import { initState } from "./initState";
import { compileToFunction } from "./compile/index";
import { callHook, mounteComponent } from "./lifecycle";
import { mergeOptions } from "./utils/index";
export function initMixin(Vue) {
  Vue.prototype._init = function (options) {
    let vm = this;
    vm.$options = mergeOptions(Vue.options, options);
    callHook(vm, 'beforeCreate')
    // 初始化状态
    initState(vm);
    callHook(vm, 'created')
    // 渲染模版
    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
  // 创建 $mount
  Vue.prototype.$mount = function (el) {
    // el template render
    let vm = this;
    el = document.querySelector(el);
    vm.$el = el
    let options = vm.$options;
    if (!options.render) {
      let template = options.template;
      if (!template && el) {
        // 这种情况下为dom模版
        // 获取outerHTML
        el = el.outerHTML;
        // 把el序列化字符串变成ast抽象语法书
        let render = compileToFunction(el);
        // console.log(render, '----render111')
        // (1)将render函数变成vnode (2)将vnode变成真实dom放到页面上去
        options.render = render
      }
    }

    // 挂载组件
    mounteComponent(vm, el)
  };
}

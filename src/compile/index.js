/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-20 16:55:30
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-24 15:42:49
 * @FilePath: /vue-create/src/compile/index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { parseHTML } from "./parseAst";
import { generate } from "./generate";
export function compileToFunction(el) {
  /**
   * 将html变成ast语法树
   */
  let ast = parseHTML(el);
  // console.log(ast, "^^^^^^^^^^^^^^^^^^^^");
  /**
   * 将ast语法树变成render函数
   * 1.ast语法树变成字符串
   * 2.字符串变成函数
   */
  let code = generate(ast);
  /**
   * 将render字符串变成函数
   */
  console.log(code, '------')
  let render = new Function(`with(this){return ${code}}`)
  // console.log(code, '------')
  console.log(render, '----render')
  /**
   * 将render函数变成vnode
   */
  return render
}

/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-20 16:55:30
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-24 14:20:15
 * @FilePath: /vue-create/src/compile/parseAst.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

// Regular Expressions for parsing tags and attributes
const unicodeRegExp =
  /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;
const attribute =
  /^\s*([^\s"'<>\/=]+)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/;
const dynamicArgAttribute =
  /^\s*((?:v-[\w-]+:|@|:|#)\[[^=]+?\][^\s"'<>\/=]*)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/;
const ncname = `[a-zA-Z_][\\-\\.0-9_a-zA-Z${unicodeRegExp.source}]*`;
const qnameCapture = `((?:${ncname}\\:)?${ncname})`;
const startTagOpen = new RegExp(`^<${qnameCapture}`);
const startTagClose = /^\s*(\/?)>/;
const endTag = new RegExp(`^<\\/${qnameCapture}[^>]*>`);
const doctype = /^<!DOCTYPE [^>]+>/i;


function parseHTML(html) {
  /**
 * 创建ast语法树对象
 * @param {*} tag
 * @param {*} attrs
 * @returns
 */
  function createASTElement(tag, attrs) {
    return {
      tag,
      attrs,
      children: [], // 子节点
      type: 1, // 元素类型
      parent: null, // 父元素
    };
  }
  let root; // 根元素
  let createParent; // 当前元素的父亲
  let stack = []; // 数据结构-栈
  /**
   *
   * @param {*} html
   * <div id="app">
   *    {{ person.name}}constructor
   *    <h1>title</h1>
   *  </div>
   */
  function start(tag, attrs) {
    // 开始标签
    let element = createASTElement(tag, attrs);
    if (!root) {
      root = element;
    }
    createParent = element;
    stack.push(element); // 入栈
  }
  function charts(text) {
    // ;
    // 文本标签
    text = text.replace(/\s/g, ""); // 正则/\s/必须添加反斜杠转义，否则代表匹配字符‘s’，而不是匹配空格
    if (text) {
      createParent.children.push({
        type: 3,
        text,
      });
    }
  }
  function end(tag) {
    // 结束标签
    let element = stack.pop(); // 出栈 获取最后一个元素
    createParent = stack[stack.length - 1];
    if (createParent) {
      // 元素的闭合
      element.parent = createParent.tag;
      createParent.children.push(element);
    }
  }
  while (html) {
    // html 为空时结束
    // 判断标签 < >
    let textEnd = html.indexOf("<");
    if (textEnd === 0) {
      /**开始标签 */
      const startTagMatch = parseStartTag(); // 拿到开始标签内容
      if (startTagMatch) {
        start(startTagMatch.tagName, startTagMatch.attrs);
        continue;
      }
      let endTagMatch = html.match(endTag);
      if (endTagMatch) {
        advance(endTagMatch[0].length);
        end(endTagMatch[1]);
        continue;
      }
    }
    /**文本标签 */
    let text;
    if (textEnd > 0) {
      // 获取文本内容
      text = html.substring(0, textEnd);
    }
    if (text) {
      charts(text);
      advance(text.length);
    }
  }
  function parseStartTag() {
    const start = html.match(startTagOpen); // return 1.匹配结果 2.false未匹配
    if (start) {
      let match = {
        tagName: start[1],
        attrs: [],
      };
      // 删除开始标签
      advance(start[0].length);
      // 获取属性
      let attr;
      let end;
      while (
        !(end = html.match(startTagClose)) &&
        (attr = html.match(attribute))
      ) {
        match.attrs.push({
          name: attr[1],
          value: attr[3] || attr[4] || attr[5],
        });
        advance(attr[0].length);
        //   break;
      }
      if (end) {
        advance(end[0].length);
        return match;
      }
    }
  }
  function advance(n) {
    html = html.substring(n);
  }
  return root;
}
export { parseHTML };

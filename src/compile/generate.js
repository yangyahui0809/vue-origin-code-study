/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-21 16:23:10
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-24 15:50:33
 * @FilePath: /vue-create/src/compile/generate.js
 * @Description:
 *
 * Copyright (c) 2023 by yangyahui, All Rights Reserved.
 */
// 插值表达式正则
const defaultTagRE = /\{\{((?:.|\r?\n)+?)\}\}/g;
/**
 * <div id="app"> hello {{msg}} <h></h></div>
 * 变成：
 * render(){
 * _c:解析标签
 * _v:解析文本
 * _s:解析data数据
 *  return _c('div',{id:"app"},_v('hello' + _s(msg)),_c)
 * }
 */

// 处理属性
function genPorps(attrs) {
  let str = "";
  for (let i = 0; i < attrs.length; i++) {
    let attr = attrs[i];
    if (attr.name === "style") {
      // 如果是内联样式 变成对象
      let obj = {};
      attr.value.split(";").forEach((item) => {
        let [key, val] = item.split(":").map(item => item.trim());
        obj[key] = val;
      });
      attr.value = obj;
    }
    // 拼接属性
    str += `${attr.name}:${JSON.stringify(attr.value)},`;
  }
  return `{${str.slice(0, -1)}}`;
}
function genChildren(el) {
  let children = el.children;
  if (children) {
    return children.map((child) => gen(child)).join(',');
  }
}

function gen(node) {
  // 3:文本 or 1: 元素
  /**
   * @param  type
   * 1: 元素
   * 2: 文本
   */
  if (node.type === 1) {
    // 如果child 是元素递归调用generate
    return generate(node);
  } else {
    /**
     * 如果是文本有两种可能
     * 1:纯文本
     * 2:包含插值表达式
     */
    let text = node.text;
    if (!defaultTagRE.test(text)) {
      // debugger
      return `_v(${JSON.stringify(text)})`;
    }
    let tokens = []
    let lastIndex = defaultTagRE.lastIndex = 0 // lastIndex 重置正则/g模式下循环匹配位置为0，再次匹配时从头开始匹配
    let match
    while (match = defaultTagRE.exec(text)) {
      lastIndex = defaultTagRE.lastIndex = 0
      let index = match.index
      if (index > lastIndex) {
        tokens.push(JSON.stringify(text.slice(lastIndex, index)))
      }
      tokens.push(`_s(${match[1].trim()})`)
      lastIndex = index + match[0].length
      if (lastIndex < text.length) {
        // tokens.push(JSON.stringify(text.slice(lastIndex)))
        text = text.slice(lastIndex)
      } else {
        // lastIndex === text.length 意味着插值表达式后面没有纯text了，此时重置lastIndex = 0 代表匹配完成
        lastIndex = 0
        break
      }
    }
    if (lastIndex) {
      // 如果lastIndex不为0代表text包含插值表达式，且表达式后有剩余裁切的纯text文本
      tokens.push(JSON.stringify(text))
    }
    return `_v(${tokens.join('+')})`
  }
}
export function generate(el) {
  //   console.log(el, "generate");
  let children = genChildren(el);
  // console.log(children,'children')
  // console.log(genPorps(el.attrs),'props')
  let code = `_c("${el.tag}",${el.attrs.length ? `${genPorps(el.attrs)}` : "undefined"}${children ? `,${children}` : ""})`;
  // console.log(code, "renderStr");
  // return 必须写，否则自调用不会获取拼接结果
  return code
}


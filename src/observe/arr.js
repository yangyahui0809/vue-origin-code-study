/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-08 16:25:49
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-23 13:12:29
 * @FilePath: /vue-create/src/observe/arr.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

// 重写数组方法
let oldAarryMethods = Array.prototype;

// 继承
let AarryMethods = Object.create(oldAarryMethods);

// 劫持
let methods = ["push", "pop", "unshift", "shift", "splice"];

methods.forEach((item) => {
  AarryMethods[item] = function (...args) {
    /**
     * 劫持数组
     */
    let inserted;
    let result = oldAarryMethods[item].apply(this, args);
    switch (item) {
      case "push":
      case "unshift":
        inserted = args;
        break;
      case "splice":
        inserted = args.splice(2); // arr.splice(0,1,{attr:xxx}) 通过splice添加对象元素
    }
    let ob = this.__ob__;
    if (inserted) {
      ob.observerArray(inserted); // 对我们添加的对象元素进行劫持
    }
    // console.log(ob.dep, 'dep in arr')
    ob.dep.notify()
    return result;
  };
});
export { AarryMethods };

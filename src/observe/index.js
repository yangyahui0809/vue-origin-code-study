/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-08 15:11:55
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-18 17:31:28
 * @FilePath: /vue-create/src/observe/index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { AarryMethods } from "./arr";
import Dep from './dep'
export function observer(data) {
  /**
   * 判断data对象是不是对象，异常优先原则；如果不是对象则不用劫持直接返回，比如string,num,null,undefined这些基础数据类型
   * 这里也是递归劫持的边界条件
   */
  if (typeof data !== "object" || data === null || data === undefined) {
    return data;
  }
  return new Observer(data);
}

/**
 * Object.defineProperty 有个缺点，只能对对象中的一个属性进行劫持，所以要想全监听必须递归遍历对象属性劫持
 * 这种递归遍历的性能很差，所以vue3弃用了这种方法
 * walk 翻译为遍历
 */
class Observer {
  constructor(value) {
    Object.defineProperty(value, "__ob__", {
      enumerable: false,
      // configurable: true,
      value: this,
      /**
       * @params value 要劫持的数据
       * 给每个要被劫持的数据 value（数组/对象）添加一个__ob__属性，指向Observer类的实例，
       * 方便在数组劫持的时候调用实例方法：observerArray，解决模块化编程后代码复用的问题
       */
    });
    this.dep = new Dep() // 给所有对象类型增加一个dep，数组本身也是一个对象
    // 判断观测的数据类型 数组 or 对象
    if (Array.isArray(value)) {
      /**数组通过函数劫持的方式写，重写数组的push等方法 */
      value.__proto__ = AarryMethods;
      this.observerArray(value);
    } else {
      this.walk(value);
    }
  }
  walk(data) {
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      // 对data对象中的每个属性进行劫持
      let key = keys[i];
      let value = data[key];
      defineReactive(data, key, value);
    }
  }
  observerArray(value) {
    for (let i = 0; i < value.length; i++) {
      observer(value[i]);
    }
  }
}
function defineReactive(data, key, value) {
  let childDep = observer(value); // 深度递归劫持
  // console.log(childDep, '----------')
  let dep = new Dep()
  Object.defineProperty(data, key, {
    get() {
      if (Dep.target) {
        // 首次处罚由vm._update(vm._render()) 中_render()函数内部_s()方法，解析插值变量处罚
        // (读取的方式是`with(this){return ${code}}`),通过with 暴露this内部的data属性
        dep.depend()
        if (childDep.dep) {
          childDep.dep.depend() // 数组收集
        }
      }
      // console.log('step--1', key)
      // debugger
      return value;
    },
    set(newValue) {
      if (newValue === value) return value;
      // 这里劫持添加的新属性（observer内部已做判断，如果设置的新属性是对象才递归）
      observer(newValue);
      value = newValue;
      dep.notify()
    },
  });
}

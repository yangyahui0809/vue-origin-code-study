/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-04-18 13:51:59
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-18 16:11:12
 * @FilePath: /vue-create/src/observe/dep.js
 * @Description: 
 * 
 * Copyright (c) 2023 by yangyahui, All Rights Reserved. 
 */
let id = 0
class Dep {
    constructor() {
        this.id = id++
        this.subs = []
    }
    // 收集watcher
    depend() {
        // 目标是:watcher可以存放dep,dep可以存放watcher ---双向记忆
        Dep.target.addDep(this)
    }
    addSub(watcher) {
        this.subs.push(Dep.target)
    }
    // 更新
    notify() {
        this.subs.forEach(watcher => {
            watcher.update()
        })
    }
}
Dep.target = null
// 添加watcher
export function pushTarget(watcher) {
    Dep.target = watcher
}
export function popTarget() {
    Dep.target = null
}
export default Dep

/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-04-18 13:34:25
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-23 16:52:39
 * @FilePath: /vue-create/src/observe/watcher.js
 * @Description: 
 * 
 * Copyright (c) 2023 by yangyahui, All Rights Reserved. 
 */
// 通过类watcher 实现更新
import { pushTarget, popTarget } from './dep'
import { nextTick } from '../utils/nextTick'

let id = 0
function flushWatcher() {
    queue.forEach(watcher => { watcher.run() /**每一个组件对应一个update */ })
    queue = []
    has = {}
    pending = false
}
class Watcher {
    // 通过new的方式调用constructor内搭方法
    constructor(vm, exprOrfn, cb, options) {
        this.vm = vm
        this.exprOrfn = exprOrfn
        this.cb = cb
        this.options = options
        this.id = id++
        this.user = !!options.user
        this.deps = [] // watcher 存放dep
        this.depsId = new Set()
        if (typeof exprOrfn === 'function') {
            this.getter = exprOrfn // 用来更新视图
        } else {
            // 如果是字符串 'a.b.c.d'
            this.getter = function () {
                let path = exprOrfn.split('.')
                let obj = vm
                for (let i = 0; i < path.length; i++) {
                    obj = obj[path[i]]
                }
                return obj

            }
        }
        // 初始化的时候
        this.value = this.get() // 自调用
    }

    addDep(dep) {
        let id = dep.id
        if (!this.depsId.has(id)) {
            this.deps.push(dep)
            this.depsId.add(id)
            dep.addSub(this)
            // console.log(dep, '---dep')
        }
    }
    run() {
        // 更新的时候走这里更新视图
        let value = this.get() // newValue
        let oldValue = this.value // oldValue
        this.value = value // 替换最新值
        // 执行handler (用户的watch)
        if (this.user) {
            // callback就是用户的handler
            this.cb.call(this.vm, value, oldValue)
        }
    }
    get() {
        pushTarget(this) // this就是Watcher的实例
        const value = this.getter() // 渲染页面
        popTarget() // 给dep 取消watcher
        return value
    }
    update() {
        queueWatcher(this)
    }
}
let queue = [] // 将需要批量更新的watcher放到一个队列中
let has = {}
let pending = false
function queueWatcher(watcher) {
    let id = watcher.id
    if (has[id] == null) { // 去重
        //  列队处理
        queue.push(watcher) // 一个组件只有一个watcher
        has[id] = true
        // 防抖处理 用户出发多次，只执行一次
        if (!pending) {
            //  异步：等待同步代码执行完毕之后，再执行异步代码块
            nextTick(flushWatcher);
        }
        pending = true
    }
}
export { Watcher }
/**
 * 收集依赖 vue dep watcher data:{name,msg}
 * vue 中更新组件的策略是：以组件为单位，给每一组件添加一个watcher,属性变化后，调用这个watcher
 * dep: dep和data中的属性一一对应
 * watch: 视图上用了几个，就有几个watcher
 * dep与watcher：一对多 dep.name = [w1,w2]
 * 对象收集依赖：dep和watcher 关系 多对多
 * 数组收集依赖：
 * 1:给所有对象类型增加一个dep，数组本身也是一个对象
 * 2:获取数组的值，会调用get方法，希望让当前数组记住这个渲染的watcher
 *  2.1:需要获取到当前的dep
 *  2.2:当前面对数组取值的时候，我们就让数组的dep记住这个watcher
 * 3：我们更新数组的时候 push 等操作，找到我们这个watcher进行更新
 */
/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-04-17 14:57:40
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-18 11:15:10
 * @FilePath: /vue-create/src/global-api/index.js
 * @Description: 
 * 
 * Copyright (c) 2023 by yangyahui, All Rights Reserved. 
 */
import { mergeOptions } from "../utils/index"
export function initGlobalApi(Vue) {
    Vue.options = {}
    Vue.Mixin = function (mixin) {
        // 对象的合并
        this.options = mergeOptions(this.options, mixin) // this是实例,mixin是全局对象
        // console.log(this.options)
        // this.options.renderTracked.forEach(fn => fn())
    }
}
/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-04-17 15:10:02
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-19 18:00:31
 * @FilePath: /vue-create/src/utils/index.js
 * @Description: 
 * 
 * Copyright (c) 2023 by yangyahui, All Rights Reserved. 
 */
const HOOKS = [
    'beforeCreate',
    'created',
    'beforeMount',
    'mounted',
    'beforeUpdate',
    'updated',
    'beforeDestroy',
    'destroyed',
    'activated',
    'deactivated',
    'errorCaptured',
    'serverPrefetch',
    'renderTracked',
    'renderTriggered'
]
let stats = {}
stats.data = function (parentVal, childVal) {
    return childVal
} // 合并data
// stats.computed = function () { } // 合并computed
// stats.watch = function () { } // 合并watch
// stats.methods = function () { } // 合并methods

// 遍历生命周期
HOOKS.forEach(hook => {
    stats[hook] = mergeHook
})
function mergeHook(parentVal, chidVal) {
    // options = { created: [a, b, c], watch: [a, b] }
    if (chidVal) {
        if (parentVal) {
            return parentVal.concat(chidVal)
        } else {
            return [chidVal]
        }
    } else {
        return parentVal
    }
}
export function mergeOptions(parent, child) {
    // console.log(parent, child)
    //  Vue.options = { created: [a, b, c], watch: [a, b] }
    const options = {}
    /**代码待优化，父亲和儿子双循环 存在同key重复赋值到问题 */
    // 有父亲没有儿子
    for (let key in parent) {
        mergeField(key)
    }
    // 儿子有父亲没有
    for (let key in child) {
        mergeField(key)
    }
    function mergeField(key) {
        // 根据key 策略模式实现
        if (stats[key]) {
            options[key] = stats[key](parent[key], child[key])
        } else {
            options[key] = child[key]
        }
    }
    return options
}


/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-04-19 13:36:29
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-23 13:12:45
 * @FilePath: /vue-create/src/utils/nextTick.js
 * @Description: 
 * 
 * Copyright (c) 2023 by yangyahui, All Rights Reserved. 
 */
let callback = []
let pending = false
function flush() {
    callback.forEach(cb => cb())
    pending = false
}
let timerFunc
// 处理兼容问题
if (Promise) {
    timerFunc = () => {
        Promise.resolve().then(flush) // 异步处理
    }
} else if (MutationObserver) { // h5异步方法
    /**
     * h5异步方法
     * 可以监听Dom变化，监控完毕之后再来异步更新
     */
    let observer = new MutationObserver(flush)
    let textNode = document.createTextNode(1) //创建文本
    observer.observe(textNode, { characterData: true }) //观测文本内容
    timerFunc = () => {
        textNode.textContent = 2
    }
} else if (setImmediate) {
    /** ie api */
    timerFunc = () => {
        setImmediate(flush)
    }
} else {
    timerFunc = setTimeout(flush, 0);
}
export function nextTick(cb) {
    // console.log(cb, '--------')
    callback.push(cb)
    if (!pending) {
        timerFunc() // 异步方法，但是要处理兼容问题
        pending = true
    }
};
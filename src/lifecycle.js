/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-04-11 13:38:05
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-23 13:34:57
 * @FilePath: /vue-create/src/lifecycle.js
 * @Description: 
 * 
 * Copyright (c) 2023 by yangyahui, All Rights Reserved. 
 */
import { Watcher } from "./observe/watcher"
import { patch } from "./vnode/patch"
export function mounteComponent(vm, el) {
    callHook(vm, 'beforeMount')
    // (1)vm._render 将render函数变成虚拟dom
    // (2)vm._update 将vnode变成真实dom放到页面上
    // vm._update(vm._render()) // 传入的参数一定是_render()的返回值 === 虚拟dom对象，而不是_render函数本身

    let updateComponent = () => {
        vm._update(vm._render())
    }
    new Watcher(vm, updateComponent, () => {
        callHook(vm, 'updated')
    }, true)
    callHook(vm, 'mounted')
}

export function lifecycleMixin(Vue) {
    Vue.prototype._update = function (vnode) {
        let vm = this
        vm.$el = patch(vm.$el, vnode)
    }
}
// 生命周期调用
export function callHook(vm, hook) {
    const handlers = vm.$options[hook]
    if (handlers) {
        for (let i = 0; i < handlers.length; i++) {
            handlers[i].call(vm) //改变生命周期this指向
        }
    }
}
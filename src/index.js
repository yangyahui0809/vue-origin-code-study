/*
 * @Author: yangyahui yangyahui@lrhealth.com
 * @Date: 2023-03-07 14:31:50
 * @LastEditors: yangyahui yangyahui@lrhealth.com
 * @LastEditTime: 2023-04-25 17:36:19
 * @FilePath: /vue-create/src/index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { initMixin } from "./init.js";
import { lifecycleMixin } from "./lifecycle.js";
import { renderMixin } from "./vnode/index.js";
import { initGlobalApi } from './global-api/index.js'
import { stateMixin } from './initState'
import { compileToFunction } from "./compile/index.js";
import { createEl, patch } from './vnode/patch'
function Vue(options) {
  this._init(options);
}
initMixin(Vue);

lifecycleMixin(Vue); // 添加生命周期 原型挂载_update
renderMixin(Vue) // 原型挂载_render
stateMixin(Vue) // 给原型添加$nextTick方法
// 全局方法 Vue[minxi | component | extend]
initGlobalApi(Vue)
/**
 * 创建虚拟dom
 */
let vm1 = new Vue({ data: { name: '张三' } })
let render1 = compileToFunction(`<ul>
<li style="background:red" key="c">c</li>
<li style="background:pink" key="b">b</li>
<li style="background:blue" key="a">a</li>
</ul>`)
let vnode1 = render1.call(vm1)
document.body.appendChild(createEl(vnode1))
// 数据更新
let vm2 = new Vue({ data: { name: '李四' } })
let render2 = compileToFunction(`<ul>
<li style="background:yellow" key="d">d</li>
<li style="background:red" key="c">c</li>
<li style="background:pink" key="b">b</li>
<li style="background:blue" key="a">a</li>
</ul>`)
let vnode2 = render2.call(vm2)
// patch 比对
console.log(vnode1, '1111')
console.log(vnode2, '2222')
patch(vnode1, vnode2)
export default Vue;
